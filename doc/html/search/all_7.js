var searchData=
[
  ['person',['Person',['../struct_person.html',1,'Person'],['../struct_person.html#a0397c6f89fafc12e738923f612bc41a3',1,'Person::Person()'],['../struct_person.html#a7816b7f20ad7ec00374090c8c42eb68f',1,'Person::Person(int age, double height, string name)']]],
  ['print',['print',['../_helpers_8h.html#a7d6d3ed44bd48cdd2b988d4071d76294',1,'Helpers.h']]],
  ['println',['println',['../_helpers_8cpp.html#a09924d963d1af8ede9841230001c31ab',1,'println():&#160;Helpers.cpp'],['../_helpers_8h.html#a09924d963d1af8ede9841230001c31ab',1,'println():&#160;Helpers.cpp'],['../_helpers_8h.html#a75c0a4c0e764cb50c148ce6f5ad84ecc',1,'println(const T &amp;t, bool tabbed=false):&#160;Helpers.h']]]
];
