
#include "Helpers.h"


bool isOnlyDigits(const std::string & input)
{
	for (const auto &c : input) {
		if (!isdigit(c)) {
			return false;
		}
	}
	return true;
}


void println()
{
	std::cout << std::endl;
}


void waitForKey()
{
	std::cout << "Press \"ENTER\" to continue: ";
	std::cin.get();
}
