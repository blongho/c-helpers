/**
 * @file	TestHelpers.cpp.
 *
 * @brief	Test any function here
 */

#include <iostream>
#include <string>
#include "Helpers.h"
using std::cout;
using std::endl;
using std::string;

/**
 * @struct	Person
 *
 * @brief	A person object to test println
 */

struct Person
{
	int age{}; /**< Age of the person*/
	double height{};/**< The person's height */
	string name{}; /**< Name of the person */
	Person() {}
	Person(int age, double height, string name) {
		this->age = age;
		this->height = height;
		this->name = name;
	}
	~Person() {}
};
std::ostream &operator<<(std::ostream &os, const Person &p);

int main()
{
	string test{ "This is a test" };
	int intNum{ 23 };
	double dbNum{ 45.3 };
	char ch{ 'C' };
	bool good{ true };
	Person p(3, 172, "Bernard");
	println("Printing stuff without tab");
	println(test);
	println(intNum);
	println(dbNum);
	println(ch);
	println(good);
	println(p);
	bool valu = static_cast<bool>(1);
	println(std::boolalpha(1 == 0));
	println();
	println("Printing stuff with tab", true);
	println(test, true);
	println(intNum, true);
	println(dbNum, true);
	println(ch, true);
	println(good, true);
	println(p, true);
	return 0;
}

std::ostream & operator<<(std::ostream & os, const Person & p)
{
	os << p.name << " is " << p.height / 100 << "m tall and " << p.age << " years old";
	return os;
}
