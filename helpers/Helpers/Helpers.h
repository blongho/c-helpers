/**
 * @file	Helpers.h.
 * @author  Bernard Longho
 * @date    2018-02-19
 * @brief   Defines some commonly used items in win32 console programs
 * Modified 2018-02-21
 */

#ifndef HELPERS_H
#define HELPERS_H  

#include <iostream>
#include <string>
#include <sstream>
#include <cctype>


 /**
 * @fn	T getNumber(const char * text, const T &min, const T &max)
 *
 * @brief	Gets a number
 * @param	text	The text to display telling the user what is required
 * @param	min 	The minimum permissible number
 * @param	max 	The maximum permissible number
 *
 * @return	The number.
 */
template<typename T>
T getNumber(const char *text, const T &min, const T &max);

/**
 * @fn	bool isOnlyDigits(const std::string &input);
 *
 * @brief	Query if 'input' is all numbers
 * @param	input	The input.
 *
 * @return	True if all characters are digits, false if not.
 */

bool isOnlyDigits(const std::string &input);

/**
 * @fn	void println();
 *
 * @brief	prints an empty line on the console
 */

void println();

/**
 * @fn	void waitForKey();
 *
 * @brief	Asks the user to press a key to continue
 **/

void waitForKey();

class String {
private:
	std::string str;
public:
	String(const std::string &st) :str{ st } {}
	~String() {}
	std::string toUpperCase() {
		for (auto &c : str) {
			if (isalpha(c)) {
				c = toupper(c);
			}
		}
		return str;
	}

	std::string toLowerCase() {
		for (auto &c : str) {
			if (isalpha(c)) {
				c = tolower(c);
			}
		}
		return str;
	}
	std::string charAt(const int &index) {
		std::string ch = "-1";
		if (index > 0 && index < static_cast<int>(str.length())) {
			ch = str[index];
		}
		return ch;
	}
};

#endif // !HELPERS_H

template<typename T>
T getNumber(const char * text, const T &min, const T &max)
{
	std::string input{};
	T num{};
	while (true) {
		std::cout << text << " [" << min << " - " << max << "]: ";
		std::getline(std::cin, input);
		std::stringstream ss(input);
		if (!isOnlyDigits(ss.str())) {
			std::cout << "Invalid input. Try again!\n";
		}
		else {
			if (isOnlyDigits(ss.str()) && ss >> num) {
				if (num >= min && num <= max) {
					break;
				}
				else {
					std::cout << "Out of range. Try again!\n";
				}
			}
		}
	}
	return num;
}


template<typename T>

/**
 * @fn	void println(const T &t, bool tabbed = false)
 *
 * @brief	Prints any type that has the << operator defined
 *
 * @param	t	  	an object to print to process.
 * @param	tabbed	prepend the print out with '\t'
 */

void println(const T &t, bool tabbed = false)
{
	std::stringstream ss;
	ss << t;
	if (tabbed) {
		std::cout << "\t";
	}
	std::cout << ss.str() << std::endl;
}

template<typename T>
void print(const T & t, bool tabbed = false)
{
	std::stringstream ss;
	ss << t;
	if (tabbed) {
		std::cout << "\t";
	}
	std::cout << ss.str();
}

